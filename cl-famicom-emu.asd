;;;; cl-famicom-emu.asd

(asdf:defsystem #:cl-famicom-emu
  :description "Describe cl-famicom-emu here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (#:cl-6502-emu)
  :components ((:file "package")
               (:file "cl-famicom-emu")))
